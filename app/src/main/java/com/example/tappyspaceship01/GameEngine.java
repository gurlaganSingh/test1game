package com.example.tappyspaceship01;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="DINO-RAINBOWS";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Player player;
    Item item;
    Item item2;
    Item item3;

    int lives = 3;
    int score;


    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // represent the TOP LEFT CORNER OF THE GRAPHIC

    // ----------------------------
    // ## GAME STATS
    // ----------------------------


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();



        this.screenWidth = w;
        this.screenHeight = h;

        this.player = new Player(getContext(), 1550, 400);
        this.item = new Item(getContext(), 100, 100);

        this.item2 = new Item(getContext(), 100, 300);
        this.item2.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.candy32));
        this.item3 = new Item(getContext(), 100, 500);
        this.item3.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.poop32));

        item2.setxPosition(item.getxPosition()+35);
        item2.setxPosition(item2.getxPosition()+15);
        item3.setxPosition(item3.getxPosition()+25);
        item.updateHitbox();


//

        this.printScreenInfo();
    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        //@TODO: Start the player at the left side of screen
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location

    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    //String value = "up";

    public void updatePositions() {

        player.updateHitbox();





        this.item.setxPosition(this.item.getxPosition()+20);
        this.item2.setxPosition(this.item2.getxPosition()+20);
        this.item3.setxPosition(this.item3.getxPosition()+20);



        int yPosition;
        int xPosition;
        Random random = new Random();
        int show =3;

        if(player.getHitbox().intersect(item.getHitbox())== true  ){

            show = random.nextInt(4) + 1;
            if(show ==1){
                xPosition = 150;
                yPosition = 0;

            }
            else if(show ==2){
                xPosition = 150;
                yPosition = 200;

            }
            else if(show ==3 ){
                xPosition = 150;
                yPosition = 400;
            }
            else{
                xPosition = 150;
                yPosition = 600;
            }
            score = score+1;


            item.setxPosition(xPosition);
            item.setyPosition(yPosition);

            player.updateHitbox();
            item.updateHitbox();

//            }


        }
//
//        if(player.getHitbox().intersect(item3.getHitbox())== true ){
//
//            lives = lives-1;
//            this.item3.setxPosition(400);
//           this.item3.setyPosition(400);
//             this.item3.updateHitbox();
//
//        }
        if(item.getxPosition() >= this.screenWidth){
            this.item.setxPosition(150);
        }
        if(item2.getxPosition() >= this.screenWidth){
                        this.item2.setxPosition(300);
               }
        if(item3.getxPosition() >= this.screenWidth){
            this.item3.setxPosition(650);
                            }


        item.updateHitbox();
            item3.updateHitbox();





        }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            // DRAW THE PLAYER HITBOX
            // ------------------------
            // 1. change the paintbrush settings so we can see the hitbox
            paintbrush.setColor(Color.BLACK);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(20);

            //----------------


//            paintbrush.setStrokeWidth(22);
            this.canvas.drawLine(100,200,1500,200,paintbrush);
            this.canvas.drawLine(100,400,1500,400,paintbrush);
            this.canvas.drawLine(100,600,1500,600,paintbrush);
            this.canvas.drawLine(100,800,1500,800,paintbrush);

            paintbrush.setStrokeWidth(5);

            paintbrush.setColor(Color.BLACK);
            paintbrush.setTextSize(50);
            canvas.drawText("Lives:" + lives,
                    1250,
                    50,
                    paintbrush
            );

            canvas.drawText("Score:" + score,
                    1550,
                    50,
                    paintbrush
            );

            paintbrush.setColor(Color.WHITE);
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);
            // draw the player's hitbox
          canvas.drawRect(player.getHitbox(), paintbrush);
            canvas.drawBitmap(item.getImage(), item.getxPosition(), item.getyPosition(), paintbrush);
            canvas.drawRect(item.getHitbox(), paintbrush);
          canvas.drawBitmap(item2.getImage(), item2.getxPosition(), item2.getyPosition(), paintbrush);
            canvas.drawRect(item2.getHitbox(), paintbrush);
            canvas.drawBitmap(item3.getImage(), item3.getxPosition(), item3.getyPosition(), paintbrush);
            canvas.drawRect(item3.getHitbox(), paintbrush);




            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(120);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------


    String fingerAction = "";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if(userAction == MotionEvent.ACTION_DOWN){

            float fingerXPosition = event.getX();
            float fingerYPosition = event.getY();

            int middle = this.screenHeight / 2;
            if (fingerYPosition >= middle) {

                if(player.getyPosition() == 600){

                }
                else
                {
                    player.setyPosition(player.getyPosition() + 200);
                }
            }
            else if (fingerYPosition < middle) {

                if(player.getyPosition() == 0){
                }
                else
                {
                    player.setyPosition(player.getyPosition() - 200);
                }

            }

        }

        return true;
    }
}
